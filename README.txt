
-- SUMMARY --

The Domain Source All module is an add on to the Domain Access suite of modules.
This module is used to make sure the domain_source table has updated values
after you switch a DA site from inactive to active. After you make a site
active, the previous nodes added need a domain_source.domain_id to match.

Other options are the affiliate content edit screen, but this will not get
paginated nodes or multiple DA sites and VBO doesn�t play well with DA in D6.

Goal:

Run batch operation to update nodes so they have domain_source.domain_id
record with correct domain_id. Currently they will not have a record,
or will have one with -5 as domain_id.

Tables involved:

domain_access: nid, gid, realm
  (realm domain_site has gid = domain_id)
domain_source: nid, domain_id
  (nodes re-saved after active get -5, otherwise no records)

Actions:

 Insert new records
   Get all nodes of a site that are in domain_access but not in domain_source

 Update existing records
   This combines both cases of not having a record or having one with -5


For a full description of the module, visit the project page:
  http://drupal.org/project/domain_send_all

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/domain_send_all


-- REQUIREMENTS --

None.
 
 
-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* User must have 'administer domains' permission.


-- TROUBLESHOOTING --

* If the menu item does not display, try clearing cache and refreshing the page.


-- CONTACT --

Current maintainers:
* Aaron Deutsch (aisforaaron) - http://drupal.org/user/116113